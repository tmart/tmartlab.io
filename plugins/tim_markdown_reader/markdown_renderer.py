import pygments
import pygments.lexers
from markupsafe import Markup
import mistletoe
import mistletoe.block_token
import logging
import re

logger = logging.getLogger(__name__)
ascii_whitespace_pattern = re.compile(r"\s+", re.ASCII)


class TimFlavoredMarkdownRenderer(mistletoe.HTMLRenderer):
    _text_lexer = pygments.lexers.get_lexer_by_name("text")

    def __init__(self):
        super().__init__()
        self.formatter_options = dict(nowrap=True)

    def render_block_code(self, token):
        code = token.children[0].content
        language = None
        if token.language:
            language = token.language
            try:
                lexer = pygments.lexers.get_lexer_by_name(token.language)
            except pygments.util.ClassNotFound:
                logger.warn(
                    f"Code block info string claimed language {language}, but Pygments doesn't have a lexer for that."
                )
                lexer = self._text_lexer
        else:
            try:
                lexer = pygments.lexers.guess_lexer(code)
                language = lexer.name.lower()
            except pygments.util.ClassNotFound:
                lexer = self._text_lexer
        formatter = pygments.formatters.html.HtmlFormatter(**self.formatter_options)

        template = "<pre><code{attr}>{inner}</code></pre>"
        if language:
            attr = ' class="{}"'.format(
                "language-{}".format(self.escape_html(token.language))
            )
        else:
            attr = ""
        inner = pygments.highlight(token.children[0].content, lexer, formatter)
        return template.format(attr=attr, inner=inner)

    def render_image(self, token):
        """
        Use figure (and figcaption) elements instead of just bare images.

        Title text will be the figcaption text if present.
        """
        figcaption_template = "<figcaption>{}</figcaption>"
        figure_template = '<figure><img src="{}" alt="{}" />{}</figure>'
        if token.title:
            figcaption = figcaption_template.format(self.escape_html(token.title))
        else:
            figcaption = ""
        return figure_template.format(
            token.src, self.render_to_plain(token), figcaption
        )

    @staticmethod
    def _make_id(contents):
        tagless = Markup(contents).striptags().lower()
        return ascii_whitespace_pattern.sub("-", tagless)

    def render_heading(self, token):
        """
        Render heading with an id and then an anchor link to that id.

        This could possibly be bad because of id collision. Ids are generated from the contents of the heading,
        which should be more or less unique under most circumstances.
        """
        template = (
            '<div class="headerlink h{level}">'
            '<h{level} id="{_id}">{inner}</h{level}><a href="#{_id}">¶</a>'
            "</div>"
        )
        inner = self.render_inner(token)
        _id = self._make_id(inner)
        return template.format(level=token.level, inner=inner, _id=_id)

    def render_paragraph(self, token):
        # I hope this doesn't break things. Would probably be better to add functionality to
        # block_token.Paragraph class
        #
        # So, usually Markdown images are rendered as <p><img /></p>.
        # But, we want to override that and use <figure> tags, which can't be placed in <p> tags,
        # so this hack removed the enclosing p tags in _just_ that instance.
        #
        # Cursory investigation shows that you gotta get deep in the internal APIs of mistletoe to
        # make an image BlockToken, so this might be the easiest way.
        if self._suppress_ptag_stack[-1] or (
            len(token.children) == 1
            and token.children[0].__class__ == mistletoe.span_token.Image
        ):
            return "{}".format(self.render_inner(token))
        return "<p>{}</p>".format(self.render_inner(token))
