import logging
from collections.abc import MutableMapping
from pathlib import Path

import mistletoe
import yaml
from lxml.html import etree, fragments_fromstring
from pelican import signals
from pelican.readers import MarkdownReader
from pelican.utils import pelican_open

from .markdown_renderer import TimFlavoredMarkdownRenderer

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

logger = logging.getLogger(__name__)


class TimMarkdownReader(MarkdownReader):
    """Reader for Markdown files with YAML metadata"""

    enabled = True

    def read(self, source_path):
        self._source_path = source_path

        with pelican_open(source_path) as source:
            content_text, metadata_text = self._split_source(source.strip())

        markdown = mistletoe.markdown(
            content_text, renderer=TimFlavoredMarkdownRenderer
        ).strip()

        metadata = dict(summary=self.summarize(markdown), slug=Path(source_path).stem)
        metadata.update(self._load_metadata(metadata_text))

        processed_metadata = {
            k: self.process_metadata(k, v) for k, v in metadata.items()
        }

        return markdown, processed_metadata

    def _split_source(self, source):
        """Split source text into metadata_text and content_text"""
        try:
            before, metadata_text, content_text = source.split("---\n", maxsplit=2)
        except ValueError:
            logger.debug(f"No YAML header found in file {self._source_path}")
            return source, {}

        if before:
            logger.debug(
                f"Possible YAML header found in file {self._source_path}, "
                f"but it did not appear at beginning of document"
            )
            return source, {}

        return content_text, metadata_text

    def _load_metadata(self, metadata_text):
        try:
            metadata = yaml.load(metadata_text, Loader)
            if not isinstance(metadata, MutableMapping):
                logger.error(f"YAML header wasn't a dict for file {self._source_path}")
                logger.debug(f"YAML data: {metadata}")
                return {}
            return metadata
        except yaml.parser.ParserError:
            logger.error(
                f"Couldn't parse YAML header of file {self._source_path}", exc_info=True
            )
            return {}

    @classmethod
    def summarize(cls, html, max_words=60):
        # TODO: just use markupsafe Markup striptags
        words_left = max_words
        paragraphs_root = etree.Element("div")
        for element in fragments_fromstring(html):
            if element.tag == "p":
                p_text = []
                for word in etree.tostring(element, method="text").split():
                    p_text.append(word.decode("utf-8"))
                    words_left -= 1
                    if not words_left:
                        p_text[-1] += "..."
                        e = etree.SubElement(paragraphs_root, "p")
                        e.text = " ".join(p_text)
                        break
                if not words_left:
                    break
                e = etree.SubElement(paragraphs_root, "p")
                e.text = " ".join(p_text)
        return etree.tostring(paragraphs_root).decode("utf-8")[5:-6]


def add_reader(readers):
    # Replace the normal Markdown readers with this one.
    for file_extension in TimMarkdownReader.file_extensions:
        readers.reader_classes[file_extension] = TimMarkdownReader


def register():
    signals.readers_init.connect(add_reader)
