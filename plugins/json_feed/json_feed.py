# -*- coding: utf-8 -*-
"""
JSON Feed Generator
===================

A Pelican plugin to generate a JSON Feed file

Heavily modified and stripped of stuff I don't use on my blog
from https://github.com/andrewheiss/pelican_json_feed
"""

import json
from markupsafe import Markup
from pelican import signals, generators, writers


class JSONFeed:
    def __init__(self, context):
        self.context = context
        self.feed = {
            "version": "https://jsonfeed.org/version/1",
            "title": context.get("SITENAME"),
            "description": context.get("DESCRIPTION"),
            "home_page_url": context.get("SITEURL_REAL"),
            "feed_url": f"{context.get('SITEURL_REAL')}{context.get('FEED_JSON')}",
            # below, kinda bad that we're hard coding an image that goes through
            # pelican processing and might change path in hard-to-determine ways
            # but i don't want to do the work to determine it correctly
            "icon": f"{context.get('SITEURL_REAL')}static/avatar.jpg",
            "favicon": f"{context.get('SITEURL_REAL')}static/avatar.jpg",
            "author": {
                "name": context.get("AUTHOR"),
                "url": context.get("SITEURL_REAL"),
                "avatar": f"{context.get('SITEURL_REAL')}static/avatar.jpg",
            },
            "items": [],
        }

    def add_item(self, **kwargs):
        self.feed["items"].append(
            {
                "id": f"{self.context.get('SITEURL_REAL')}{kwargs.get('link')}",
                "url": f"{self.context.get('SITEURL_REAL')}{kwargs.get('link')}",
                "title": kwargs.get('title'),
                "content_html": kwargs.get('content'),
                "summary": Markup(kwargs.get('description')).striptags(),
                "date_published": kwargs.get('pubdate').isoformat(),
            }
        )

    def write(self, fp, *args):
        json.dump(self.feed, fp, indent=4)


class JSONFeedGenerator(generators.ArticlesGenerator):
    def generate_feeds(self, writer):
        """Generate the feeds from the current context, and output files."""

        if self.settings.get("FEED_JSON"):
            writer.write_feed(
                elements=self.articles,
                context=self.context,
                path=self.settings["FEED_JSON"],
                feed_type="json",
            )

    def generate_output(self, writer):
        self.generate_feeds(writer)
        signals.article_writer_finalized.send(self, writer=writer)


class JSONFeedWriter(writers.Writer):
    def _create_new_feed(self, feed_type, feed_title, context):
        if feed_type != "json":
            return super()._create_new_feed(feed_type, feed_title, context)
        return JSONFeed(context)


def get_generators(generators):
    return JSONFeedGenerator


def get_writer(writer):
    return JSONFeedWriter


def register():
    signals.get_writer.connect(get_writer)
    signals.get_generators.connect(get_generators)
