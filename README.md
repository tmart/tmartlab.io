# Tim's Blog

Here's my site.

## Package Management

I maintain a set of packages for production and another for development.

### Production

```
pip install -r requirements.txt
```

### Development

```
pip install -r requirements.txt
pip install -r requirements-dev.txt
```

### Upgrading/Changing Package Versions

This project is trying out [`pip-tools`](https://github.com/jazzband/pip-tools) for deterministic and secure package management. It's listed in the `requirements-dev.txt` file, so just `pip install` that to obtain in your environment

To update a package (after running the above):

```
pip-compile --upgrade --generate-hashes
pip-compile --upgrade --generate-hashes requirements-dev.in
```

There are more usage scenarios covered in the `pip-tools` documentation.
