import itertools
import calendar


def articles_by_month(articles, month_fmt):
    yield from _articles_by_time(articles, "%Y-%m", month_fmt)


def articles_by_year(articles, year_fmt):
    yield from _articles_by_time(articles, "%Y", year_fmt)


def _articles_by_time(articles, key_fmt, ret_fmt):
    # assume articles are already date sorted... otherwise, results are meaningless
    for _, article_group in itertools.groupby(articles, lambda a: a.date.strftime(key_fmt)):
        article_group = list(article_group)
        yield article_group[0].date.strftime(ret_fmt), article_group


def month_name(month_number):
    return calendar.month_name[int(month_number)]


def truncate_index_html(url):
    token = 'index.html'
    if url.endswith(token):
        return url[:-len(token)]
    return url


def debug(v):
    """ Set a breakpoint"""
    breakpoint()
    return v
