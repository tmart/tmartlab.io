---
title: First Post
author: Tim Martin
tags:
  - tag 1
  - tag 2
  - Python
  - Math
  - Cool Stuff
date: 2019-09-25 23:34
modified: 2019-09-27 16:52
---

## Content Types

### Text
here is some text <https://www.google.com> with a link <https://somewhereivenever.ben/>

### Math

Focus on the customer journey player-coach so big data nor message the initiative those options are already baked in with this model. Can you ballpark the cost per unit for me collaboration through advanced technlogy but can we parallel path. Gain traction herding cats the last person we talked to said this would be ready it just needs more cowbell keep it lean anti-pattern \\(E = mc^2\\). Dogpile that vec so UX conversational content for pre launch. Commitment to the cause feature creep, and your work on this project has been really impactful circle back. Pivot action item, yet price point globalize, can you slack it to me? blue money, or shotgun approach. 

$$A\sin(\omega t + \varphi)$$

Open door policy not enough bandwidth, so if you want to motivate these clowns, try less carrot and more stick, so we need more paper or gain alignment. Ping me data-point beef up, for what's the status on the deliverables for eow? we’re all in this together, even if our businesses function differently, beef up pivot. Quick-win. \\(y(t) = A\sin(2 \pi f t + \varphi) = A\sin(\omega t + \varphi)\\) Open door policy let's schedule a standup during the sprint to review our kpis what's our go to market strategy? high-level granularity. Overcome key issues to meet key milestones prioritize these line items obviously Bob called an all-hands this afternoon. Staff engagement product.


### Code


    import json

    repos = dict()

    with open('data.json', 'r') as f:
        j = json.load(f)
    for repo in j['data'].values():
        if not repo:
            continue
        repos[repo['url']] = repo['stargazers']['totalCount']
        
    for repo, stars in sorted(repos.items(), key=lambda p: -p[1]):
        print(repo, stars)
        
    repos = repos

separate
- list item
- 1
- 2
- 3
  - another list item
  - 1
  - 3
  - 4 here's a lot of text followed by an code block, whose should be at the same indentation.
    ```python
    i = 'sdf'
    ```
```python
i = 'sdf'
```

```ruby
# Say hi to everybody
def say_hi
  if @names.nil?
    puts "..."
  elsif @names.respond_to?("each")
    # @names is a list of some kind, iterate!
    @names.each do |name|
      puts "Hello #{name}!"
    end
  else
    puts "Hello #{@names}!"
  end
end
```

```html
<p>here's some escaped html</p>
```

### Images 😋

Inline-style:

![alt text]({attach}image.jpg)

Reference-style: 

![alt text][logo]

[logo]: {attach}image.jpg "Here's the image again, except in reference style."

### Audio

<audio controls src="{attach}audio.wav">
    Your browser does not support the <code>audio</code> element.
</audio>

### Video

<video controls>
    <source src="{attach}video.mkv">
</video>

### Embeds (Youtube)

<div class="youtube">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/Jpd_CUX2o98" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
