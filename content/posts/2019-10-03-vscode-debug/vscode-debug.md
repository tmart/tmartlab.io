---
title: Visual Studio Code Debugging Tips
date: 2019-10-03 00:00
---

I recently figured out how to do some cool things with the [Visual Studio Code](https://code.visualstudio.com/) debugger. I did them with Python code, but you may find usefulness outside of that.

## Breakpoint Debugging into Third-Party Packages

If you create a default launch configuration, you can't set breakpoints into other packages/modules. The UI will let you do it, but upon execution, those breakpoints will become "unverified".

![Unverified Breakpoint in VSCode]({attach}unverified.png "Unverified Breakpoint? ...Pretty unhelpful.")

Instead, you need to tell VSCode to expand breakpoint triggering by turning off the <strong><code>justMyCode</code></strong> option in your launch configurations. Add something like this to your launch.json:

```json
{
    "name": "Python: File",
    "type": "python",
    "request": "launch",
    "program": "${file}",
    "justMyCode": false
}
```

You can now run your code and set breakpoints in anything you import and have a peek around.

I think it's kinda bad that this option is `true` by default; breakpoints are already an opt-in thing, but now I have to opt-in twice. (Maybe there's some performance gains by turning off debugging in these sections of code?)

## Debugging Python Modules/Command Line Tools

Modules like pip can be run from the command line. Either running `python -m pip` or simply `pip` will run that module.

But, what if you wanted to debug that module in VSCode, with breakpoints and all?

You can do just that by setting up a **module** launch configuration. Here's an example:

```json
{
    "name": "Pip list",
    "type": "python",
    "request": "launch",
    "module": "pip",
    "args": [
        "list",
        "--verbose"
    ],
    "justMyCode": false
}
```

Two important things there:
- `"module": "pip"` is used instead of `"program": "foo"`. This works for any module that has a `__main__.py` file. This is the same as running `python -m pip`. (If you want to see how just the `pip` command hooks up to the pip module, check out the entrypoints in `setup.py` in any module, such as [this one](https://github.com/pypa/pip/blob/e6f69fadd8c84526d933b2650141193595059e72/setup.py#L75).)
- `"justMyCode": false` like in the previous tip if you want to set breakpoints.

## Breakpoint Types
There's more than just the pause-and-see type of breakpoint in VSCode.

Instead of left-clicking on the breakpoint section of a line, try right-clicking. You'll see a few options:
- **Expression** allows you to define a condition from that scope in the code. The breakpoint will only trigger if that condition evaluates truthfully
- **Hit Count** takes in a positive integer that defines the number of times this breakpoint is encountered before pausing the debugger. As far as I know, expressions like `3 * 5` don't produce meaningful values. Stick with just numbers.
- **Log Expression** will print your variable-interpolated string to the terminal each time the breakpoint is encountered. It does not pause the debugger.

## `breakpoint()` Statements
I'm not sure how useful this is, but I thought it was cool. If you're debugging in VSCode and encounter a [`breakpoint()`](https://docs.python.org/3/library/functions.html#breakpoint) statement, the breakpoint will trigger in VSCode.

Outside of VSCode, this usually calls `pdb.settrace()`, [Python's built-in textual debugger](https://docs.python.org/3/library/pdb.html). But, as said by the `breakpoint()` docs, if you override `sys.breakpointhook`, you can call whatever code you want.

This is why when you run Python in the debugger, the file that first gets run is something like `ptvsd_launcher.py`: it's setting `sys.breakpointhook` and then running your code, which comes in as an argument.