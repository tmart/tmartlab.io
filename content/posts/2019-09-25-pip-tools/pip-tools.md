---
title: pip-tools 😍, Pipenv 🤔
author: Tim Martin
date: 2019-09-25 00:00
---

I just stumbled upon [pip-tools](https://github.com/jazzband/pip-tools): What a great idea!

```bash
$ cat requirements.in
pelican
pyyaml
mistletoe
pygments
lxml

$ pip-compile --generate-hashes
$ cat requirements.txt
blinker==1.4 \
    --hash=sha256:471aee25f3992bd325afa3772f1063dbdbbca947a041b8b89466dc00d606f8b6 \
    # via pelican
docutils==0.15.2 \
    --hash=sha256:6c4f696463b79f1fb8ba0c594b63840ebd41f059e92b31957c46b74a4599b6d0 \
    --hash=sha256:9e4d7ecfc600058e07ba661411a2b7de2fd0fafa17d1a7f7361cd47b1175c827 \
    --hash=sha256:a2aeea129088da402665e92e0b25b04b073c04b2dce4ab65caaa38b7ce2e1a99 \
    # via pelican
...
```

It's glue between pip and a [version-pinned](https://nvie.com/posts/pin-your-packages/) requirements.txt file.
This is important because:

> ... you can never know what you’ll get when you run `pip install`.

> The only way of making your builds deterministic, is if you pin every single package dependency (even the dependency’s dependencies).

pip-tools is causing me to reconsider my use of [pipenv](https://github.com/pypa/pipenv), which also captured my heart about a year ago.
Pipenv is like pip-tools, but also manages your virtual environment. To achieve version pinning, it stores hashes in it's own file format in a Pipfile.lock file.

But:
- I haven't come across many popular projects that use Pipenv in the wild. Please show me some if you know of any. I believe this might be because the most popular projects in the Python ecosystem are libraries, while Pipenv is targeted towards end users' projects?
- As of writing, the lastest version of Pipenv is v2018.11.26, or about 10 months old. This isn't necessarily a bad thing, but compared to [the past more-rapid release history of the project](https://github.com/pypa/pipenv/blob/e70b5e53b3cab2f400fe42bf99a3e13dc72ef631/CHANGELOG.rst), I'm wondering "What's up?".
- Finally and perhaps most importantly, Pipenv introduces another layer of abstraction. Instead of working through virtual environments and pip directly, you instead use Pipenv. I think this might be a hard pill to swallow for some people.
  - With pip-tools, version pinning is automatically in the requirements.txt file, an [official](https://docs.python.org/3/installing/index.html) primitive of Python packages, by way of pip's support for them. requirements.txt files are seen practically everywhere.

I'm going to give pip-tools a try for a while. Yes, I'll have to manage my virtual environment directly, but I do not think this is hard (`python -m venv .venv`, activate it, and you're done).