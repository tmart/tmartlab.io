#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
from pathlib import Path
sys.path.append(str(Path('.')))
from pelicanconf import *  # noqa

SITEURL = SITEURL_REAL = "https://tim.direct/"

GOOGLE_ANALYTICS = "UA-34180881-4"

DELETE_OUTPUT_DIRECTORY = True
