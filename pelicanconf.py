#!/usr/bin/env python
# -*- coding: utf-8 -*- #
import sys
from pathlib import Path

# Without setting this, the `pelican` command will not find project-level imports
sys.path.append(str(Path(".")))

from filters import (  # noqa: E402
    articles_by_month,
    articles_by_year,
    debug,
    month_name,
    truncate_index_html,
)
from plugins import project_plugins  # noqa: E402


AUTHOR = "Tim Martin"
SITENAME = "Tim Martin™"
DESCRIPTION = f"The site of {AUTHOR}"
# these pelican jokers are stripping away trailing slashes from SITEURL, so create an untouched one.
SITEURL = SITEURL_REAL = "/"
EXTRA_PATH_METADATA = {
    "static/favicon.ico": {"path": "favicon.ico"},
    "static/robots.txt": {"path": "robots.txt"},
}

TIMEZONE = "America/Chicago"
DEFAULT_LANG = "en"

PATH = Path("content")
THEME = Path("theme")
PLUGINS = project_plugins
JINJA_FILTERS = {
    "articles_by_year": articles_by_year,
    "articles_by_month": articles_by_month,
    "month_name": month_name,
    "truncate_index_html": truncate_index_html,
    "debug": debug,
}
STATIC_PATHS = ["static"]

ARTICLE_PATHS = [d.relative_to(PATH) for d in (PATH / "posts").iterdir() if d.is_dir()]

INDEX_URL = "blog"
INDEX_SAVE_AS = "blog/index.html"

ARCHIVES_URL = "blog/archives"
ARCHIVES_SAVE_AS = "blog/archives/index.html"

ARTICLE_URL = "blog/posts/{slug}"
ARTICLE_SAVE_AS = "blog/posts/{slug}/index.html"

TEMPLATE_PAGES = {"home.html": "index.html", "resume.html": "resume/index.html"}

DIRECT_TEMPLATES = ["index", "archives"]

PAGINATION_PATTERNS = (
    (1, "{url}", "{save_as}"),
    (2, "{base_name}/{number}/", "{base_name}/{number}/index.html"),
)

LINKS = (
    ("Blog", "blog", f"{INDEX_SAVE_AS}"),
    ("Archives", "archives", f"{ARCHIVES_SAVE_AS}"),
    ("Resume", "resume", "resume"),
)

AUTHOR_SAVE_AS = CATEGORY_SAVE_AS = TAGS_SAVE_AS = TAG_SAVE_AS = PAGE_SAVE_AS = False

FEED_JSON = "feed.json"

FEED_ATOM = FEED_RSS = None
CATEGORY_FEED_ATOM = CATEGORY_FEED_RSS = None
AUTHOR_FEED_ATOM = AUTHOR_FEED_RSS = None
TAG_FEED_ATOM = TAG_FEED_RSS = None
FEED_ALL_ATOM = FEED_ALL_RSS = None

DISQUS_SITENAME = "tim-martin"

DELETE_OUTPUT_DIRECTORY = False

DEFAULT_PAGINATION = 3

NEWEST_FIRST_ARCHIVES = True

RELATIVE_URLS = True
